"""
Created on Fri Nov 10 10:22:24 2023

@author: mlecadet
"""

import re

def extraction_dt_format(e):
    # ajouter un format si nécessaire
    formats = [r'\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}', r'\d{2}\d{2}\d{2}\d{2}\d{2}\d{2}', r'\d{4}-\d{2}-\d{2}T\d{2}-\d{2}-\d{2}', r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}', r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', r'\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}', r'\d{4}_\d{2}_\d{2}T\d{2}_\d{2}_\d{2}']

    match = None
    for f in formats:
        match = re.search(f, e)
        if match:
            break
    if match:
        if f == r'\d{4}-\d{2}-\d{2}T\d{2}-\d{2}-\d{2}':
            dt_format = '%Y-%m-%dT%H-%M-%S'
        elif f == r'\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}':
            dt_format = '%Y-%m-%d_%H-%M-%S'
        elif f == r'\d{2}\d{2}\d{2}\d{2}\d{2}\d{2}':
            dt_format = '%y%m%d%H%M%S'
        elif f == r'\d{2}\d{2}\d{2}_\d{2}\d{2}\d{2}':
            dt_format = '%y%m%d_%H%M%S'
        elif f == r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}':
            dt_format = '%Y-%m-%d %H:%M:%S'
        elif f == r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}':
            dt_format = '%Y-%m-%dT%H:%M:%S'
        elif f == r'\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}':
            dt_format = '%Y_%m_%d_%H_%M_%S'
        elif f == r'\d{4}_\d{2}_\d{2}T\d{2}_\d{2}_\d{2}':
            dt_format = '%Y_%m_%dT%H_%M_%S'
        return f, dt_format
    else:
        raise ValueError(f'{e}: No datetime found')
