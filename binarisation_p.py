# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 09:43:47 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION « binarisation »

# L'objectif de cette fonction est de prendre en argument une matrice ("mat1" ici) et de passer tous ses coefficients à 0 au 1, selon qu'ils sont inférieurs ou supérieurs à un "seuil" à définir. Le résultat est donc une matrice binaire.


## ENTREES

# matrice = matrice quelconque (il s'agit dans notre cas de "mat1")

# valeur_max = valeur maximale pour la translation homothétique et la construction de la matrice "mat1"

# y_min, y_max = équivalents en coordonnées matricielles de "f_min" et "f_max"

# pourcentage = pourcentage_seuil_energetique = proportion des valeurs les moins énergétiques à supprimer pour la construction de la matrice "mat_binaire"

# l= longueur_image_spectro = longueur de la matrice "mat1", vu comme une image

## SORTIES

# mat_binaire = matrice binaire, image de "mat1" dont a simplement passé les coefficients à 0 au 1, selon qu'ils sont inférieurs ou supérieurs au "seuil"

# countscum = histogramme cumulatif des valeurs de "mat1"

# x = vecteur en abscisse des toutes les valeurs entières entre 0 et "valeur_max" (cf. fonction « homothetie »)

# +
import numpy as np

def binarisation(matrice, valeur_max, y_min, y_max, pourcentage, longueur_image_spectro):
    [counts, x] = np.histogram(matrice, bins=(valeur_max+1)) # histogramme simple de la matrice "mat1"
    countscum = np.cumsum(counts)/(longueur_image_spectro*(y_max-y_min)) # histogramme cumulatif
    # On décide arbitrairement de supprimer "prop"=80% des valeurs les moins énergétiques.
    # On ignore donc au moins 80% des valeurs aux intensités les plus faibles. D'où :
    seuil = 0 # initialisation de la valeur du seuil qui, via la boucle « for » qui suit, prendra sa valeur
            # finale entre 0 et 50000.
    for p in range(0,(valeur_max+1)):
        if countscum[p]>pourcentage/100:
            seuil=p
            break
    # Désormais, on décide de faire la discrimation suivante : les coefficients de la matrice "mat1" supérieurs
    # au seuil établi sont associés au booléen «True», ceux situés en deçà sont associés à «False».
    # On obtient ainsi la matrice binaire "mat_binaire" :
    mat_binaire = (matrice > seuil)
    return mat_binaire, countscum, x

