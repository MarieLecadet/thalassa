# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 15:51:33 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION « modeles_inter »

# L'objectif de cette fonction est de fournir, au choix, 3 ondelettes-mères différentes pour l'intercorrélation.
# Ces ondelettes-mères sont de mêmes durée et fréquence d'échantillonnage que le signal avec lequel elles seront ensuite
# intercorrélées.


## ENTREES (pour les trois sous-fonctions)

# N = nombre d'échantillons qui composent le vecteur "sig" (cf. « thalassa_main »), c'est-à-dire le signal de départ

# t_tot = durée totale du signal étudié "sig"


## SORTIE (pour les trois sous-fonctions)

# ondelette-mère

# +
import numpy as np

# Modèle 1 : version stricte de la définition de l'onde de Gabor (gaussienne*cosinus)

def gabor_1(N,t_tot):
    T = np.linspace(0, t_tot, N)
    f_gabor = 40000 # Fréquence centrale de l'onde de Gabor ex : 40 kHz
    sigma_gabor = 0.00001  # Écart-type de l'onde de Gabor ex : 10 microsecondes
    onde_gabor = np.exp(-(T-t_tot/2) ** 2 / (2 * sigma_gabor ** 2))*np.cos(2 * np.pi * f_gabor * (T-t_tot/2))
    return onde_gabor

# Modèle 2 : onde de Gabor modulée par une exponentielle décroissante, modèle lisse pour approcher au mieux
# la réalité théorique

def gabor_2(N,t_tot):
    f_gabor_bis = 40000 # Fréquence centrale de l'onde de Gabor ex : 40 kHz
    sigma_gabor_bis = 2e-5  # Écart-type de l'onde de Gabor ex : 20 microsecondes
    T = np.linspace(0, t_tot, N)
    fct_1 = np.cos(2*np.pi*f_gabor_bis*(T-(5)))/15
    fct_2 = np.exp(-(T-(t_tot/2)**2/(2*sigma_gabor_bis**2)))*10/15
    fct_3 = np.concatenate((np.ones(int(N/2)), np.exp(-74000*(T-(t_tot/2))[int(N/2)::])), axis=None)
    f = fct_1*fct_2*fct_3*150
    return f

# Modèle 3 : modèle basé sur une approche statistique des enregistrements audio pour approcher au
# mieux la réalité terrain L'objectif de ce modèle est de palier l'incompatibilité entre la fréquence
# d'échantillonnage "Fe" du signal de départ et le modèle 2 (en effet, dans le modèle 2, en passant
# de N à N_bis, l'échantillonnage trop aléatoire, ne rend pas compte correctement de la forme d'onde
# que l'on souhaite reproduire). On définit donc la fonction "gabor_ter" qui prend en argument "Fe" et "N".

def gabor_3(N,t_tot):
    moy_periode = 2.66e-5   # période moyene au sein d'un seul clic, entre les différents maxima
    frequence_onde = 1/moy_periode    # fréquence associée à "moy_periode"

    F_mini = 2*frequence_onde  # on comprend qu'il faut au minimum 2 données par période pour la création de cette onde
    # en effet, il faut : 1 point pour le maximum et un point pour le minimum pour chaque période, on aura donc bien une onde
    # d'aspect triangulaire
    Fe = round(N/t_tot)
    
    num = round(Fe/F_mini)  # rapport entier entre "Fe" et "F_mini" qui détermine le nombre de points par demi-période lors
    # de la création de l'onde "onde_gabor_ter"

    # L'expérience montre que le second pic d'un clic possède toujurs la plus grande amplitude, on s'en sert donc comme référence
    # pour définir les hauteurs relatives des autres pics (pic 1, pic 3 et pic 4) :
                   
    rel1 = 0.28585  # moyenne de la hauteur relative entre l'amplitude du premier pic et celle du second pic
    rel3 = 0.62505  # moyenne de la hauteur relative entre l'amplitude du troisième pic et celle du second pic
    rel4 = 0.27160  # moyenne de la hauteur relative entre l'amplitude du quatrième pic et celle du second pic

    onde=[0, 0, rel1/2]
    h_relatives=[rel1,1, rel3, rel4]

    if num==1:
        onde=[0, 0, rel1/2, -rel1/2, 0.5, -0.5, rel3/2, -rel3/2, rel4/2, -rel4/2]
    else :
        for p in range(4):
            for k in range(num):
                onde.append(onde[-1]-h_relatives[p]/2)
            if p!=3 :
                onde.append(h_relatives[p+1]/2)
    onde.append(0)
    onde.append(0)
    onde = np.concatenate((np.zeros(int((N-len(onde))/2)), onde, np.zeros(int((N-len(onde))/2))), axis=None)
    return onde

