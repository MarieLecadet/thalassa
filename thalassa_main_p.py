# -*- coding: utf-8 -*-
# +
"""
Created on Tue Sep 12 09:15:24 2023

@author: lecadema
"""

# IMPORTATION DES BIBLIOTHEQUES ET FONCTIONS NECESSAIRES A L'IMPLEMENTATION DU CODE

# Packages et fonctions pré-existantes
import os
import numpy as np
import time
import re
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.patches import Rectangle
from datetime import datetime, timedelta
from scipy.io import wavfile
from scipy.signal import correlate
from csv import writer
# Fonctions créées pour Thalassa
from bloc_traitement_p import bloc_traitement
from extraction_p import extraction
from calcul_spectro_p import calcul_spectro
from format_date_p import extraction_dt_format

# %% «TOP HORAIRE» POUR MESURE DU TEMPS D'EXECUTION DE L'ALGORITHME

depart = time.time()

# LISTE DES PARAMETRES A RENSEIGNER AVANT LE LANCEMENT DU SCRIPT

params = ['3', 85, 0.3, 9, 140, 1, 6, 15e3, 130e3, '+02', 1, 'campagne_trux']

# params[0] = modèle-mère utilisé pour l'intercorrélation
# params[1] = seuil énergétique : 'pourcentage_seuil_energetique'
# params[2] = écart inter-clics maximal (au-delà duquel on distingue deux trains de clics différents) : 'ICI_max'
# params[3] = nombre de pics sur la somme
# params[4] = hauteur de l'élément structurant clic_morph : 'h_morph'
# params[5] = largeur de l'élément structurant clic_morph : 'l_morph'
# params[6] = largeur minimale d'un pic sur la somme : 'larg_min'
# params[7] = fréquence minimale d'observation du spectrogramme : 'f_min'
# params[8] = fréquence maximale d'observation du spectrogramme : " f_max = min(Fe/2, params[8]) "
# params[9] = décalage horaire entre le fuseau de l'enregistrement et l'UTC
# params[10] = '1' si l'on veut récupérer les spectros avec les boîtes, '0' sinon
# params[11] = nom de la campagne dont est issu le dataset, qui sera rappelé dans le nom du csv

# CREATION DE LA LISTE DES FICHIERS A TRAITER
# 2 options : en local ou sur le réseau DATARMOR

# dossier = '/home/datawork-osmose/dataset/APOCADO_C2D1_IROISE_07072022/data/audio/10_144000'
# dossier = '/home/datawork-osmose/dataset/APOCADO_C6D3_ST7178/data/audio/10_128000'
# dossier = '/home/datawork-osmose/dataset/APOCADO_C6D3_ST7180/data/audio/10_128000'
# dossier = '/home/datawork-osmose/dataset/CETIROISE_POINT_A_28082022/data/audio/10_128000'
dossier = '/Users/Utilisateur/Documents/academique/EN_Travail/sciences/VA_ASM/PFE/PFE_poursuite/inconnus'

L_fichiers = []
l_dossier = len(dossier)

for fichier in os.listdir(dossier):
    if fichier.endswith(".wav"):
        L_fichiers.append(dossier+'/'+fichier)
L_fichiers.sort()

L_fichiers = L_fichiers[0:30]

# %% Création du dossier de stockage des spectros, si nécessaire
# Ce dossier sera placé dans le même répertoire que le srcipt "thalassa_main"

if params[10] == 1:
    os.mkdir('spectros_positifs_im')

# %% CREATION DU CSV REPRENANT LES RESULTATS

# Création du nom du csv
nom_csv = 'thalassa_'+params[11]+'_modele_'+str(params[0])+'_'+str(params[1])+'_'+str(params[2])+'_'+str(params[3])+'_'+str(params[4])+'x'+str(params[5])+'_'+str(params[6])+'_'+str(params[7])+'_'+str(params[8])+'.csv'
# Création de l'entête du csv
entete=['dataset','filename','start_time','end_time','start_frequency','end_frequency','annotation','annotator','start_datetime','end_datetime','is_box']
# Création du csv vide, avec entête
with open(nom_csv, 'w+', newline='') as f_object:
    writer_object = writer(f_object)
    writer_object.writerow(entete)
    f_object.close()

# %% BOUCLE DE TRAITEMENT

# Indice pour quantifier le nombre de positifs obtenus à la fin du traitement du dataset :

nb_positifs = 0

for y in range(len(L_fichiers)):

    if y % 10 == 0:
        print('Nombre de fichiers analysés jusqu\'ici: ', y+1)

    # Récupération des caractéristiques du fichier audio :
    # durée totale ; fréquence d'échantillonnage ; longueur vectorielle
    Fe, sig = wavfile.read(L_fichiers[y])
    N = len(sig)
    t_tot = round(N/Fe)

    # Définition liminaire de la plage d'observation fréquentielle
    # NB : la condition « y == 0 » permet de ne le calculer
    # qu'une seule fois, lors du traitement du premier fichier
    if y == 0:
        f_min = params[7]
        f_max = min(Fe/2, params[8])

    # Pour que l'algorithme puisse fonctionner si le signal de l'hydrophone
    # est enregistré en stéréo, on pose une condition pour ne conserver que
    # le premier canal du signal :
    if len(np.shape(sig)) > 1:
        sig = sig[:, 0]

    # Création du vecteur temps, support temporel en secondes
    T = np.linspace(0, t_tot, N)

    # Calcul du modèle de Gabor choisi pour l'intercorrélation
    # NB : la condition « y == 0 » permet de ne le calculer
    # qu'une seule fois, lors du traitement du premier fichier
    if y == 0:
        if params[0] == '1':
            from modeles_inter_p import gabor_1
            modele = gabor_1(N, t_tot)
        if params[0] == '2':
            from modeles_inter_p import gabor_2
            modele = gabor_2(N, t_tot)
        if params[0] == '3':
            from modeles_inter_p import gabor_3
            modele = gabor_3(N, t_tot)

    # Intercorrélation du signal d'origine par le modèle précédemment calculé
    inter = correlate(sig, modele, mode='same')

    # Récupération de la liste contenant les coordonnées des trains de clics
    L, l, f, t, liste_vert = bloc_traitement(inter, N, Fe, f_min, f_max, valeur_max=50000, pourcentage_seuil_energetique=params[1], ICI_max=params[2], nb_clics_min=params[3], h_morph=params[4], l_morph=params[5], larg_min=params[6])

    # On ignore la suite de la boucle si L est vide, c'est-à-dire si
    # aucun train de clics n'est détecté. On passe immédiatement au
    # fichier audio suivant.
    if L == []:
        continue

    # Incrémentation du compteur de détections positives
    nb_positifs += 1

    # Si L n'est pas vide, on récupère donc les intervalles
    # temporels contenant les trains
    extraits, _ = extraction(L, N, t_tot, sig, l, params[2])
    N_ex = len(extraits)

    # REMPLISSAGE DU CSV

    for p in range(N_ex):

        # Extraction du format de date :

        f, date_format = extraction_dt_format(L_fichiers[y])

        # Récupération de la date d'enregistrement du fichier audio
        match = re.search(f, L_fichiers[y])
        if match:
            date_str = match.group()

        # Bloc de rédaction de la ligne «synthèse» pour le fichier audio
        if p == 0:
            s_dbt = 0
            s_fin = 10
            res_dbt = datetime.strptime(date_str, date_format) + timedelta(seconds=s_dbt)
            res_dbt = str(res_dbt)
            res_fin = datetime.strptime(date_str, date_format) + timedelta(seconds=s_fin)
            res_fin = str(res_fin)
            D_dbt = res_dbt[:10]+'T'+res_dbt[11:23]+'.000'+params[9]+':00'
            D_fin = res_fin[:10]+'T'+res_fin[11:23]+'.000'+params[9]+':00'
            liste_resultats = [params[11], L_fichiers[y][l_dossier+1::], 0, 10, 0, f_max, 'Odontocete click', 'thalassa', D_dbt, D_fin, 0]
        with open(nom_csv, 'a',newline='') as f_object:
            writer_object = writer(f_object)
            writer_object.writerow(liste_resultats)
            f_object.close()

        # Bloc de rédaction de la ligne associée à chaque boîte
        s_dbt = extraits[p][0][0]
        s_fin = extraits[p][0][-1]
        res_dbt = datetime.strptime(date_str, date_format) + timedelta(seconds=s_dbt)
        res_dbt = str(res_dbt)[0:23]
        res_fin = datetime.strptime(date_str, date_format) + timedelta(seconds=s_fin)
        res_fin = str(res_fin)[0:23]
        if res_dbt[-4] == '.':
            D_dbt = res_dbt[:10]+'T'+res_dbt[11:23]+params[9]+':00'
        else:
            D_dbt = res_dbt[:10]+'T'+res_dbt[11:23]+'.000'+params[9]+':00'
        if res_fin[-4] == '.':
            D_fin = res_fin[:10]+'T'+res_fin[11:23]+params[9]+':00'
        else:
            D_fin = res_fin[:10]+'T'+res_fin[11:23]+'.000'+params[9]+':00'
        liste_resultats = [params[11], L_fichiers[y][l_dossier+1::], extraits[p][0][0], extraits[p][0][-1], liste_vert[p][1], liste_vert[p][0], 'Odontocete click', 'thalassa', D_dbt, D_fin, 1]
        with open(nom_csv, 'a',newline='') as f_object:
            writer_object = writer(f_object)
            writer_object.writerow(liste_resultats)
            f_object.close()

        if params[10] == 1:
            f, t, Sxx_sig = calcul_spectro(sig, Fe, 512, 0.6)
            plt.pcolormesh(t, f, 20*np.log10(abs(Sxx_sig)), shading='Gouraud', cmap=cm.jet)
            plt.xlim([0, 10])
            plt.xticks([], [])
            plt.yticks([], [])
            # affichage de la boîte contenant le train de clics, sur le spectrogramme
            # COIN EN BAS à gauche, puis largeur, puis hauteur
            for p in range(N_ex):
                largeur = extraits[p][0][-1]-extraits[p][0][0]
                hauteur = liste_vert[p][1]-liste_vert[p][0]
                plt.gca().add_patch(Rectangle((extraits[p][0][0], liste_vert[p][0]), largeur, hauteur, fc='none', ec='k', lw=2))
            plt.savefig('spectros_positifs_im'+L_fichiers[y][l_dossier:-3]+'png', dpi=300)
            plt.close()

# %% RETOUR DU TEMPS D'EXECUTION, DU NOMBRE DE FICHIERS AUDIO TRAITES, LES NOMS DU PREMIER ET DERNIER FICHIER AUDIO TRAITE

temps = time.strftime("%H:%M:%S", time.gmtime(time.time()-depart))

print('\nTemps total d\'exécution : \n', temps)
print('Nombre total de fichiers dans le dataset analysé : \n ', len(L_fichiers))
print('Nombre total de fichiers positifs à la détection : \n ', nb_positifs)
print('Premier enregistrement analysé : \n ', L_fichiers[0][l_dossier+1::])
print('Dernier enregistrement analysé : \n ', L_fichiers[-1][l_dossier+1::])
