# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 09:19:26 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION "bloc_traitement"

# L'objectif de cette fonction, est de partir d'un signal de départ et de retourner une liste des intervalles qui
# contiennent les potentiels trains de clics de ce signal. Dans notre cas, ce signal d'entrée est le résultat de
# l'intercorrélation du signal de départ "sig" par l'une des ondelettes-mères. On note que la liste retournée contient
# des intervalles matriciels, qui seront ensuite traduits en intervalles temporels par la fonction « extraction »


## ENTREES

# signal = signal temporel (résultat de l'intercorrélation du signal de départ "sig" par l'une des ondelettes-mères)

# N = nombre d'échantillons qui composent le vecteur "signal", c'est-à-dire le signal de départ

# Fe = fréquence d'échantillonnage de "signal"

# f_min = fréquence basse d'observation du spectrogramme

# f_max = fréquence haute d'observation du spectrogramme

# valeur_max = valeur maximale pour la translation homothétique et la construction de la matrice "mat1"

# pourcentage_seuil_energetique = proportion des valeurs les moins énergétiques à supprimer pour la construction de
# la matrice "mat_binaire"

# ICI_max = intervalle de temps théorique MAXIMAL potentiellement observé entre 2 clics consécutifs, au sein d'un même
# train Prendre en compte ce paramètre biologique implique de distinguer deux potentiels trains de clics différents
# lorsque cet écart est dépassé entre 2 clics consécutifs


## SORTIES

# L = liste de stockage des intervalles de temps présentant potentiellement des clics

# l= longueur_image_spectro = longueur du spectrogramme, vu comme une image. On note que, logiquement, c'est aussi la
# longueur en abscisse de "mat2", et de "somme"

# f = vecteur fréquentiel en ordonnée de la représentation du spectrogramme

# t = vecteur temporel en abscisse de la représentation du spectrogramme

# limites_verticales_boites_spectro = valeurs limites de la hauteur des boîtes de détection de trains

# +
import numpy as np
import cv2 as cv
from calcul_spectro_p import calcul_spectro
from homothetie_p import homothetie
from binarisation_p import binarisation
from detection_trains_p import detection_trains

def bloc_traitement(signal,N,Fe, f_min, f_max,valeur_max,pourcentage_seuil_energetique,ICI_max,nb_clics_min, h_morph, l_morph, larg_min):
    t_tot = round(N/Fe)

    # Choix pour notre méthode : filtre avec une bande-passante entre 20kHz et 130kHz.
    # On comprend donc qu'un signal échantillonné à une fréquence inférieure à 40kHz ne permettra pas
    # de détecter des clics.
    # On se propose de sortir de l'algorithme en cas de non respect de cette condition :
    if Fe < 40e3 :
        print("Fréquence d'échantillonnage trop faible")
        print(exit())

    ### CALCUL DU SPECTROGRAMME

    f, t, Sxx = calcul_spectro(signal, Fe, 2048, 0.95)

    l = np.shape(Sxx)[1]    # échelle temporelle : longueur du spectogramme vu comme une image
    h = np.shape(Sxx)[0]    # échelle fréquentielle : hauteur du spectogramme vu comme une image

    # Les bornes fréquentielles "f_min" et "f_max" qui nous intéressent sont des paramètres en arguments de
    # la fonction. "y_min" et "y_max" sont leur exact équivalent en coordonnées matricielles pour sélectionner
    # la bande du spectrogramme qui nous intéresse.

    y_min = round(f_min*h/f_max)
    y_max = h

    mat = Sxx[y_min:y_max,:] # "mat" correspond à la sélection du spectrogramme sur l'intervalle
                             # [y_min, y_max]
                             # NB : "mat" est une matrice, traitée comme une image

    ### TRANSLATION HOMOTHETIQUE

    # Pour revenir à des méthodes de traitement d'image classiques et permettre l'utilisation des
    # fonctions Python associées, on décide, par une opération homothétique, de passer des valeurs
    # rélles d'amplitude de l'hydrophone à des valeurs comprises entre 0 et "valeur_max" :
    valeur_max = 50000
    # Ainsi, par translation, la valeur minimale fournie par l'hydrophone devient 0. Et la valeur
    # maximale devient "valeur_max". Ceci permet, pour la suite, un traitement de la matrice "mat1"
    # comme une image en nuances de gris, par valeurs entières.
    # On passe les amplitudes des valeurs réelles du spectrogramme en valeurs entières situées entre 0
    # et "valeur_max" (plus l'intensité du signal est élevée, plus la valeur réelle du pixel est
    # élevée et donc plus la valeur entière qui lui est désormais affectée se rapproche de
    # "valeur_max")

    mat1 = homothetie(mat, valeur_max)  # matrice dont les valeurs sont comprises entre 0 et "valeur_max"

    ###  OBTENTION DE LA MATRICE BINAIRE

    # La fonction « binarisation » va permettre, de façon automatique, de déterminer le seuil
    # pertinent qui permettra de ne conserver que les extraits de signal présentant une énergie
    # suffisante.
    # Le postulat émis consiste à considérer simplement que, grâce à leur intensité énergétique,
    # les clics de dauphins se démarquent de l'environnement sonore dans lequel ils sont émis.
    # Dans notre cas, cela se traduit directement par la conservation des coefficients dont la valeur
    # se situe au-dessus du seuil à fixer entre 0 et "valeur_max" = 50000.
    # L'implémentation automatique se fait par la construction de l'histogramme cumulatif, qui
    # représente la distribution des différentes valeurs au sein de la matrice "mat1".

    # À cette fin, on décide arbitrairement de supprimer "prop"% des valeurs les moins énergétiques.
    prop=pourcentage_seuil_energetique

    mat_binaire, countscum, x = binarisation(mat1, valeur_max, y_min, y_max, prop, l)


    ### SÉLECTION DES INTERVALLES DE TEMPS PENDANT LESQUELS LE SIGNAL PRÉSENTE
    ### DES VALEURS SUPÉRIEURES AU SEUIL ÉNERGÉTIQUE PRÉCÉDEMMENT FIXÉ PAR OPÉRATIONS MORPHOLOGIQUES

    # Création de l'élément structurant qui permettra d'extraire les clics : ceux-ci, sur un
    # spectrogramme, sont représentés par de longs traits verticaux. Notre élément structurant
    # "clic_morph" sera donc une ligne de hauteur "h_morph" et de largeur "l_morph".

    clic_morph = cv.getStructuringElement(cv.MORPH_RECT,(l_morph, h_morph))

    # En faisant l'intersection entre l'image binaire "mat_binaire" et notre élément structurant
    # "clic_morph" on obtient la matrice "mat2" qui correspond à "mat_binaire" dépollué (suppression
    # des échos, tâches, etc )

    mat2 = cv.morphologyEx(np.float32(mat_binaire), cv.MORPH_OPEN, clic_morph)

    ### RECUPERATION DES INTERVALLES TEMPORELS D'INTERET
    # Il s'agit dans ce qui suit, de ne conserver que les intervalles de temps sur lesquels, après ces
    # premières étapes de filtrage, on considère qu'il y a potentiellement des clics de dauphins

    # Pour faciliter la récupération de ces intervalles, on construit un vecteur "somme" qui permet de
    # mettre en valeur les colonnes de "mat2" présentant des valeurs non nulles (et donc potentiellement
    # des clics)

    somme=np.sum(mat2,axis=0)

    ### SÉLECTION DES PLAGES DE TEMPS PRÉSENTANT POTENTIELLEMENT DES CLICS

    # La fonction « detection_trains » va permettre la récupération des intervalles de temps réels
    # des zones d'intérêt, i.e. les plages temporelles du signal d'origine où l'on considère, à l'issue des
    # différents traitements précédents, qu'il y a potentiellement des clics.

    L, abscisse_clics = detection_trains(somme, l, t_tot, ICI_max,nb_clics_min, larg_min)

    # Ces quelques dernières lignes permettent d'obtenir les limites haute et basse de chaque boîte

    h_mat2 = np.shape(mat2)[0]
    limites_verticales_boites_spectro=[]

    for p in range (len(abscisse_clics)):
        val_min_train, val_max_train = h_mat2, 0
        for e in abscisse_clics[p]:
            v_min, v_max = 0, h_mat2-1
            while mat2[v_min][e]==False and v_min<h_mat2-1:
                v_min+=1
            while mat2[v_max][e]==False and v_max>0:
                v_max-=1
            val_min_train = min(v_min, val_min_train)
            val_max_train = max(v_max, val_max_train)
        c_bas = np.round(f_min+((f_max-f_min)/h_mat2)*val_min_train)
        c_haut = np.round(f_min+((f_max-f_min)/h_mat2)*val_max_train)
        limites_verticales_boites_spectro.append([c_bas, c_haut])

    return L,l,f,t,np.array(limites_verticales_boites_spectro)
