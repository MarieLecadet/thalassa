# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 10:57:48 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION « homothétie »

# Cette fonction prépare l'exploitation de la matrice "mat" comme une image. Elle transforme la valeur minimale de "mat"
# en 0 et sa valeur maximale en "valeur_max". Toutes ses valeurs intermédiaires sont translatées sur l'intervalle
# [ 0 ; valeur_max ], de façon à respecter la distribution initiale des valeurs. Cette opération est réalisée par commodité
# de représentation et afin de pouvoir ensuite travailler avec des valeurs entières suffisamment discriminantes.


## ENTREES

# matrice = matrice quelconque (dans notre cas il s'agit de "mat", c'est-à-dire la sélection du spectrogramme entre f_min
# et f_max)

# valeur_max = valeur maximale, choisie arbitrairement haute, pour la translation homothétique et la construction de la
# matrice "mat1" (sortie de la fonction "homothetie")


## SORTIES

# mat1 = matrice image de "mat", mais dont chaque valeur a subi une translation homothétique

# +
import numpy as np

def homothetie(matrice, valeur_max):
    m = np.min(matrice) # plus petite amplitude réelle de la matrice "mat"
    M = np.max(matrice) # plus grande amplitude réelle de la matrice "mat"
    mat1 = np.round(matrice*valeur_max/(M-m) + valeur_max*m/(M-m)) # matrice aux valeurs comprises entre 0 et valeur_max
    return mat1

