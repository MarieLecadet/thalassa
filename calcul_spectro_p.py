# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 09:43:47 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION « calcul_spectro »

# Le rôle de cette fonction est de retourner le spectrogramme d'un signal, en choisissant ses paramètres de calcul.

## ENTREES

# sig = signal temporel dont on veut calculer le spectrogramme

# Fe = fréquence d'échantillonnage de "sig" (cf. module « main »)

# npeseg = nombre de points par segments pour le calcul du spectrogramme

# taux_recouvrement = proportion de recouvrement de "npeseg" pour ajuster la précision temporelle
# que l'on souhaite pour le calcul du spectrogramme


## SORTIES

# f = vecteur fréquentiel en ordonnée de la représentation du spectrogramme

# t = vecteur temporel en abscisse de la représentation du spectrogramme

# Sxx = matrice du spectrogramme

# +
from scipy import signal

def calcul_spectro(sig, Fe, npseg, taux_recouvrement):
    f, t, Sxx = signal.spectrogram(sig, Fe, nperseg = npseg, window=('hamming'), noverlap=round(taux_recouvrement*npseg))
    return(f, t, Sxx)
