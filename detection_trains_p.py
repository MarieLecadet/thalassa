# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 12:11:36 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION « detection_trains »

# L'objectif de la fonction « detection_trains » est, à partir de la lecture du vecteur "somme" (cf. module «bloc_1»),
# de statuer finalement sur la présence ou l'absence de trains de clics au sein d'un enregistrement.
# Cette fonction est le « juge de paix » de l'algorithme Thalassa.
# Puisqu'une image vaut mille mots, nous vous renvoyons, pour une compréhension plus concrète de son fonctionnement, vers
# le tableau 1 (p.34) du rapport associé à ce projet. Cette illustration vous donnera une idée plus précise de l'influence
# des différents critères d'entrée.


## ENTREES

# somme = vecteur en 1D qui fournit, pour chaque colonne de la matrice "mat2" (cf. module «bloc_1»), la somme
# totale des coefficients de la colonne ( on rappelle que ceux-ci valent 0 ou 1 )

# longueur_image_spectro = longueur matricielle du spectrogramme, vu comme une image. On note que, logiquement,
# c'est aussi la longueur des variables "mat2", et de "somme" (cf. module «bloc_1»)

# t_tot = durée totale du signal étudié (en secondes)

# ICI_max = intervalle de temps théorique MAXIMAL potentiellement observé entre 2 clics consécutifs, au sein d'un même train
# Prendre en compte ce paramètre biologique implique de distinguer deux potentiels trains de clics différents lorsque
# cet écart est dépassé entre 2 clics consécutifs

# nb_clics_min = nombre minimal de pics rouges (c’est-à-dire nombre minimal de clics dans le cas idéal où aucun autre
# type d’impulsion n’est conservé) à partir duquel on considère qu’une succession de clics peut être considérée comme
# un réel train

# larg_min =  largeur minimale que doit présenter la base d’un pic sur l'histogramme du vecteur "somme" (cf. figure 27
# p.32 du rapport) pour être considéré comme un vrai clic. Ceci permet d’éliminer les signaux d’intensité plus faible


## SORTIES

# L = liste de stockage des intervalles matriciels présentant potentiellement des clics

# L_ABX_CLICS = liste qui, À CHAQUE TRAIN DÉTECTÉ ET RECONNU COMME TEL SELON NOS CRITÈRES, va associer
# la liste "l_abx_clics_1train", elle-même regroupant les abscisses "abx_clic" où l'on pourra trouver les clics
# NB : on pourra noter que "L" et "L_ABX_CLICS", ont la même longueur : celle-ci correspond au nombre 
# de trains de clics détectés, à  l'issue des premiers traitements
# -

def detection_trains(somme, longueur_image_spectro, t_tot, ICI_max,nb_clics_min, larg_min):

    # Par définition, l'intervalle entre les clics (ICI), au sein d'un même train, ne dépasse jamais ICI_max.
    # On utilise alors le compteur "compt" pour quantifier l'écart temporel entre deux clics consécutifs. Si
    # "compt" dépasse la valeur "delta" (équivalent matriciel d'ICI_max qui, on le rappelle, est une donnée
    # temporelle), ceci marque la fin du train précédemment entamé et réinitialise l'algorithme pour préparer
    # la détection du début du train suivant :
    compt=0
    delta = round(ICI_max*longueur_image_spectro/t_tot)

    L=[]        # liste qui stockera des intervalles de temps présentant des clics
    debut=0     # variable qui reprend le début (temporel) de chaque train
    fin=0       # variable qui reprend la fin (temporelle) de chaque train

    nb_clics=0  # compteur qui permet d'ignorer volontairement les trains constitués de moins de
                # "nb_clics_min" clics (les clics isolés sont généralement associés à des clics de
                # crustacés)

    # Plus tard, on voudra également récupérer les limites verticales qui vont encadrer les trains de clics.
    # Cela suppose tout d'abord de trouver l'abscisse de chaque clic, au sein de chaque train. À ce titre,
    # "L_ABX_CLICS" est une liste de liste qui, À CHAQUE TRAIN DÉTECTÉ ET RECONNU COMME TEL SELON NOS CRITÈRES,
    # va associer la liste "l_abx_clics_1train", elle-même regroupant les abscisses "abx_clic" où l'on pourra
    # trouver chacun des clics.
    # NB : Les abscisses récupérées sont relatives aux abscisses du vecteur "somme" (cf. module «bloc_1»)
    l_abx_clics_1train=[]
    L_ABX_CLICS = []
    
    abx_val_max_1train, maximum = 0, 0
    compt = -1

    for k in range(1, longueur_image_spectro-1):
        compt += 1
        if somme[k] == 0:
            if compt < delta:
                if somme[k-1] == 0:
                    continue
                else:
                    if compt <= larg_min:
                        nb_clics -= 1
                        if nb_clics == 0:
                            debut, fin, compt, l_abx_clics_1train = 0, 0, 0, []
                        else:
                            fin, compt = k-1, 0
                    else:
                        fin, compt = k, 0
            else:
                if debut == 0:
                    compt = 0
                else:
                    if nb_clics < nb_clics_min:
                        debut, fin, compt, l_abx_clics_1train = 0, 0, 0, []
                    else:
                        L.append([debut, fin])
                        L_ABX_CLICS.append(list(l_abx_clics_1train))
                        debut, fin, compt, l_abx_clics_1train = 0, 0, 0, []
        else:
            if somme[k-1] == 0:
                if debut == 0:
                    abx_val_max_1train, maximum, debut, compt, nb_clics = k, somme[k], k, 0, 1
                else:
                    if nb_clics == 0:
                        debut = k
                        compt = 0
                        nb_clics += 1
                        l_abx_clics_1train.append(abx_val_max_1train)
                        abx_val_max_1train, maximum = k, somme[k]
                    else:
                        if min(k-debut, k-fin) <= delta:
                            compt = 0
                            nb_clics += 1
                            if somme[k] > maximum:
                                abx_val_max_1train, maximum = k, somme[k]
                        else:
                            debut, fin, compt, l_abx_clics_1train = 0, 0, 0, []
            else:
                if somme[k] > maximum:
                    abx_val_max_1train, maximum = k, somme[k]
                if compt == delta:
                    compt = 0

    # En sortie de boucle, la dernière condition ci-après permet de gérer les trains de clics qui s'étendent
    # jusqu'au terme du signal de départ, sans poser de problème de taille de liste :
    
    if debut == 0 or nb_clics < nb_clics_min:
        None
    else:
        L.append([debut, fin])
        L_ABX_CLICS.append(l_abx_clics_1train)

    return L, L_ABX_CLICS

