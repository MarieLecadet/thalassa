# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 14:26:35 2023

@author: lecadema
"""

# +
## PRINCIPE DE FONCTIONNEMENT DE LA FONCTION « extraction »

# Le rôle de cette fonction est de passer des coordonnées matricielles aux abscisses temporelles réelles qui encadrent
# les clics. En effet, la liste "L" contient les coordonnées matricielles des limites qui encadrent les trains de clics
# sur le vecteur "somme". Pour remplir le CSV, il faut désormais re-traduire ces coordonnées matricielles en intervalles
# temporels : on extrait de "L" l'information temporelle recherchée à la base.


## ENTREES

# L = liste de stockage des intervalles de temps présentant potentiellement des clics

# N = nombre d'échantillons qui composent le vecteur "sig" (cf. module « main »), c'est-à-dire le
# signal de départ

# t_tot = durée totale du signal temporel étudié (en secondes)

# sig = signal temporel de départ (cf. module « main »)

# longueur_image_spectro = longueur du spectrogramme, vu comme une image. On note que,
# logiquement, c'est aussi la longueur en abscisse de la matrice "mat2", et du vecteur "somme"

# ICI_max = intervalle de temps théorique MAXIMAL potentiellement observé entre 2 clics consécutifs, au sein d'un même train
# Prendre en compte ce paramètre biologique implique de distinguer deux potentiels trains de clics différents lorsque
# cet écart est dépassé entre 2 clics consécutifs


## SORTIE

# extraits = liste qui contient les bornes des intervalles temporels qui contiennent les trains de clics
# NB : "extraits" sera donc une liste de listes

# +
import numpy as np

def extraction(L, N, t_tot, sig, longueur_image_spectro, ICI_max):
    extraits=[]
    fin=None
    for k in range(0,len(L)):
        debut=max(round(L[k][0]*N/longueur_image_spectro),0)
        fin=min(round(L[k][1]*N/longueur_image_spectro),N)
        tau=np.linspace(debut,fin,fin-debut+1)*t_tot/N
        if (fin-debut)*t_tot/N > ICI_max:
            if fin!=N:
                extraits.append([tau, sig[debut:fin+1]])
            else:
                extraits.append([tau, sig[debut-1:fin+1]])
    return extraits, fin    # la variable "fin" est retournée à des fins de commodité d'affichage
                            # N'étant pas une réelle donnée de sortie, elle n'apparaît pas dans
                            # le paragraphe « SORTIE »

