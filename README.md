# Thalassa : détecteur automatique de clics de dauphin commun 

## DESCRIPTION GENERALE DU PROJET

Le projet Thalassa a pour objectif le développement d'un script Python qui assurerait la détection automatique de trains de clics de dauphins communs (_Delphinus delphis_) au sein d'enregistrements audio. Il propose une alternative au détecteur F-POD développé par l'entreprise CHELONIA LDT.
NB : le code Python principal (« **thalassa_main** ») consiste en une boucle qui traite les uns après les autres tous les fichiers .WAV stockés dans un dossier choisi (cf. partie INSTRUCTION POUR L'UTILISATION).

## STATUT DU PROJET

Développé en trois mois, ce détecteur automatique en est à sa version 1. Il pourra encore profiter de futures modifications qui permettront peut-être d'améliorer ses performances (précision, rappel, F-score).

## ENVIRONNEMENT D'EXECUTION

Le fonctionnement d'un code Python nécessite presque toujours l'utilisation de bibliothèques et de fonctions référencées. 
Le script « thalassa_main » ne fait pas exception et nécesite les 9 bibliothèques suivantes : **os, re, csv, numpy, time, matplotlib, scipy, datetime, time**.

Ainsi, pour le lancer en local, il suffit d'importer toutes ces bibliothèques. 

Pour le lancer sur Datarmor, il faut s'assurer de travailler dans un environnement virtuel Python qui contienne bien toutes les bibliothèques nécessaires à son bon fonctionnement. À cette fin, un environnement nommé " **thalassa_env** " a été créé : il contient toutes les bibliothèques nécessaires à l'implémentation du code.
Cet environnement est disponible sur DATARMOR, infrastructure de calcul HPC de l'institut IFREMER, à Plouzané (29).
Une fois assuré(e) que vous avec un compte DATARMOR, vous pouvez accéder aux serveurs de DATARMOR _via_ un tunnel SSH.
Cet accès est permis par le lancement du fichier PBS « **launcher.pbs** », par l'intermédiaire de la commande BASH "**qsub**", depuis un Terminal : "**qsub launcher.pbs**"

L'activation de l'environnement " **thalassa_env** " est permise dans le fichier PBS par la ligne 9 :

« **conda activate /home/datawork-osmose/conda-env/thalassa_env** »

/!\ Assurez-vous d'avoir les droits d'accès exigés /!\

## INSTRUCTIONS D'UTILISATION SUR DATARMOR

### Etape 1 : transformer le fichier PBS

Il est nécessaire de renseigner l'espace utilisateur depuis lequel vous travaillez. Autrement dit, il vous que précisiez l'adresse du coin de serveur dans lequel vous avez rangé le code principal « **thalassa_main** », ses 7 fonctions associées ainsi que le fichier PBS. 
NB : prenez bien garde à mettre ces 9 fichiers au même endroit !

Ceci fait, il faut que vous adaptiez les lignes **4**, **5** et **11** du fichier PBS :

Ligne 4) **PBS -o /home4/datahome/brichard/PFE_code_VF/resultats.out** 

Ligne 5) **PBS -e /home4/datahome/brichard/PFE_code_VF/error.out**

Ligne 11) **python /home4/datahome/brichard/PFE_code_VF/thalassa_main.py**

Pour chacune de ces trois lignes, à la place du chemin « **/home4/datahome/brichard/PFE_code_VF** », il vous suffit de mettre le chemin de votre propre espace d'exécution.

Votre fichier PBS est prêt !

### Etape 2 : transformer le script « **thalassa_main** »

Il y a 2 transformations à effectuer au sein du script Python « **thalassa_main** » :

La première consiste à ajuster, ligne 3 de la cellule 2 pour une ouverture _via_ Notebook, la liste des paramètres d'entrée :
« params = ['3', 85, 0.3, 9, 140, 1, 6, 15e3, 130e3, '+01', 1, 'nom_de_la_campagne'] ».
Le rôle de ces 12 paramètres est décrit juste à la suite, dans le script. Pour une plus ample compréhension de leur influence, nous vous invitons à lire le rapport de ce projet.


La deuxième concerne le chemin d'accès à la base de données que vous souhaitez traiter, ligne 40 de la cellule 2 pour une ouverture _via_ Notebook. Il faut remplacer la valeur de la chaîne de caractère « **dossier** » par le bon chemin :
**dossier = '/home/datawork-osmose/dataset/APOCADO_C6D3_ST7178/data/audio/10_128000'**
Il suffit d'associer à « **dossier** » le chemin du bon répertoire de données préalablement stocké sur DATARMOR.
Encore une fois, attention aux droits d'accès !


## INSTRUCTIONS D'UTILISATION EN LOCAL

En local, pas besoin de fichier PBS, il suffit de réaliser 2 transformations dans le script Python « **thalassa_main** » : les paramètres et le chemin du dataset à tester (cf étape 2 des instructions d'utilisation sur Datarmor).

## Comparaison du CSV de résultats avec un CSV de référence (annotation manuelle) : s'assurer de la compatibilité des CSV

Si vous souhaitez comparer le CSV résultat de « **thalassa_main** » à un autre CSV d'annotation manuelle, prenez garde à une chose :

/!\ « **thalassa_main** » détecte la plupart des buzz comme étant des trains de clics ! /!\

Pour pallier cet écueil, il vous suffit de suivre les étapes suivantes :
1) Créer une copie du CSV X
2) Ouvrir cette copie sur Excel
3) Crl+f → Aller dans l'onglet "Remplacer"
4) Remplacer le mot "buzz" par le mot "click"
5) Cliquer sur "Remplacer partout"
6) Fermer

Vous pouvez désormais comparer le CSV résultat de Thalassa à la copie du CSV X : la comparaison sous le label "click" incluera également de manière implicite les buzz.

## CE PROJET EST LIBRE DE DROIT, N'HESITEZ PAS A NOUS CONTACTER SI VOUS AVEZ DES QUESTIONS OU DES SUGGESTIONS !
